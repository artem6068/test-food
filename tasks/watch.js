import gulp from 'gulp';

gulp.task('watch', () => {
  gulp.watch(['./src/components/**/*.json', './src/components/**/*.pug', './src/**/*.pug'], gulp.series('pug:json', 'pug'));
  gulp.watch('./src/**/*.scss', gulp.series('sass'));
  gulp.watch('./src/**/*.js', gulp.series('js'));
  gulp.watch('./src/static/img/*', gulp.series('copy'));
});
