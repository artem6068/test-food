import gulp from 'gulp';

import browserSync from 'browser-sync';

gulp.task('copy', () => {
  return gulp.src('./src/static/img/*')
    .pipe(gulp.dest('./app/static/img/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});
