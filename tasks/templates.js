import gulp from 'gulp';

import pug from 'gulp-pug';
import mergeJson from 'gulp-merge-json';
import data from 'gulp-data';

import browserSync from 'browser-sync';
import notify from 'gulp-notify';
import fs from 'fs';

gulp.task('pug:json', () => {
  return gulp.src(['./src/components/**/*.json'])
    .pipe(mergeJson({
      fileName: 'data.json'
    }))
    .pipe(gulp.dest('./src/temp'))
    .pipe(browserSync.reload({
      stream: true
    }));
})

gulp.task('pug', () => {
  return gulp.src('./src/pages/*.pug')
    .pipe(data(file => {
      return JSON.parse(fs.readFileSync('./src/temp/data.json'));
    }))
    .pipe(pug({
      pretty: true,
    }))
    .on("error", notify.onError({
      message: "<%= error.message %>",
      title: "Error pug"
    }))
    .pipe(gulp.dest('./app'))
    .pipe(browserSync.reload({
      stream: true
    }));
});
